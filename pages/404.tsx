import React from 'react';
import Link from 'next/link';
import { Typography } from 'antd';

const NotFound = () => {
  return (
    <div className="wrapper mx-auto flex flex-col items-center text-center">
      <Typography.Title>404 - Page Not Found</Typography.Title>
      <Link href="/">
        <a className="text-lg underline hover:underline">
          Go back home
        </a>
      </Link>
    </div>
  );
};

export default NotFound;
