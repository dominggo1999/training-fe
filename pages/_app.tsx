import type { AppProps } from 'next/app';
import {
  QueryClientProvider,
  QueryClient,
} from '@tanstack/react-query';
import { persistQueryClient, removeOldestQuery } from '@tanstack/react-query-persist-client';
import { createSyncStoragePersister } from '@tanstack/query-sync-storage-persister';
import 'antd/dist/antd.css';
import '../styles/main.scss';
import Head from 'next/head';

function MyApp({ Component, pageProps }: AppProps) {
  // Create a client
  const queryClient = new QueryClient({
    defaultOptions: {
      queries: {
        cacheTime: 1000 * 60 * 60 * 24, // 24 hours
      },
    },
  });

  if (typeof window !== 'undefined') {
    const localStoragePersister = createSyncStoragePersister({
      storage: window.localStorage,
      retry: removeOldestQuery,
    });

    persistQueryClient({
      queryClient,
      persister: localStoragePersister,
    });
  }

  return (
    <QueryClientProvider client={queryClient}>
      <Head>
        <link
          rel="icon"
          href="/icons/favicon.ico"
        />
        <link
          rel="apple-touch-icon"
          href="/icons/apple-touch-icon.png"
        />
        <link
          rel="manifest"
          href="/manifest.json"
        />
      </Head>
      <Component {...pageProps} />
    </QueryClientProvider>
  );
}

export default MyApp;
