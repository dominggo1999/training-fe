import React from 'react';
import useMovies from '../hooks/useMovies';
import PageTitle from '../components/PageTitle';
import MovieList from '../components/MovieList';
import Loading from '../components/Loading';

const Home = () => {
  const { isLoading, isError, data: movies } = useMovies();

  if (isLoading) return <Loading />;

  if (isError) {
    return <p>ERROR</p>;
  }

  return (
    <div className="wrapper">
      <PageTitle>Popular Movies</PageTitle>
      <MovieList movies={movies} />
    </div>
  );
};

export default Home;
