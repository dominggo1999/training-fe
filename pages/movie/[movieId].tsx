import React from 'react';
import { Button } from 'antd';
import { ArrowLeftOutlined } from '@ant-design/icons';
import Link from 'next/link';
import Loading from '../../components/Loading';
import useMovieDetails from '../../hooks/useMovieDetails';
import MovieDetails from '../../components/MovieDetails';

const Movie = () => {
  const {
    isLoading, isError, getDetails,
  } = useMovieDetails();

  if (isLoading) return <Loading />;
  if (isError) return <p>Error</p>;

  const details = getDetails();

  // getDetails will always return undefined if there is no movie and always proceed with redirection to 404 page, while checking, show loader before redirecting
  if (!details) {
    return <Loading />;
  }

  return (
    <div className="wrapper">
      <div className="flex justify-end">
        <Link href="/">
          <Button
            className="mb-8"
            type="primary"
            size="large"
            icon={<ArrowLeftOutlined />}
          >
            Go Back
          </Button>
        </Link>
      </div>
      <MovieDetails {...details} />
    </div>
  );
};

export default Movie;
