/* eslint-disable camelcase */
import React from 'react';
import Image from 'next/image';
import { Typography } from 'antd';
import { MovieData } from '../../hooks/useMovies';
import { getPosterUrl } from '../../utils/url';
import useFallbackImageSrc from '../../hooks/useFallbackImageSrc';

const MovieDetails = (
  {
    title, overview, poster_path, unoptimizeImage = false,
  }: MovieData & Partial<{ unoptimizeImage: boolean }>,
) => {
  const { src, fallback, fallbackUnoptimized } = useFallbackImageSrc(getPosterUrl(poster_path));

  return (
    <div className="movie-details">
      <div className="movie-details__image-container">
        <Image
          unoptimized={unoptimizeImage || fallbackUnoptimized}
          objectFit="cover"
          layout="fill"
          alt="example"
          src={src}
          onError={fallback}
        />
      </div>
      <div className="movie-details__content">
        <Typography.Title
          level={1}
        >
          {title}
        </Typography.Title>
        <Typography.Paragraph>{overview}</Typography.Paragraph>
      </div>
    </div>
  );
};

export default MovieDetails;
