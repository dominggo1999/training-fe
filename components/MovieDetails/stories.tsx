import MovieDetails from '.';

const mockMovieData = {
  adult: false,
  backdrop_path: '/5GA3vV1aWWHTSDO5eno8V5zDo8r.jpg',
  genre_ids: [
    27,
    53,
  ],
  id: 760161,
  original_language: 'en',
  original_title: 'Orphan: First Kill',
  overview: 'After escaping from an Estonian psychiatric facility, Leena Klammer travels to America by impersonating Esther, the missing daughter of a wealthy family. But when her mask starts to slip, she is put against a mother who will protect her family from the murderous “child” at any cost.',
  popularity: 4696.941,
  poster_path: '/pHkKbIRoCe7zIFvqan9LFSaQAde.jpg',
  release_date: '2022-07-27',
  title: 'Orphan: First Kill',
  video: false,
  vote_average: 6.8,
  vote_count: 1013,
};

export const Basic = () => {
  return (
    <MovieDetails
      {...mockMovieData}
      unoptimizeImage
    />
  );
};

export default {
  title: 'Movie Details',
};
