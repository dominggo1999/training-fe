import React, { useEffect, useState } from 'react';
import MovieCard from '.';
import { MOVIE_BASE_URL } from '../../constants/endpoints';

const sampleMovieCardData = {
  poster: '/AeyiuQUUs78bPkz18FY3AzNFF8b.jpg',
  title: 'Fullmetal Alchemist: The Final Alchemy',
  releaseDate: '2022-06-24',
  voteAverage: 6.1,
  id: '960704',
  unoptimizeImage: true,
};

const LimitWidth = ({ children }: { children: React.ReactElement }) => {
  return (
    <div className="max-w-[300px]">
      {children}
    </div>
  );
};

export const Default = () => {
  return (
    <LimitWidth>
      <MovieCard
        {...sampleMovieCardData}
      />
    </LimitWidth>
  );
};

export const ImageError = () => {
  return (
    <LimitWidth>
      <MovieCard
        {...{
          ...sampleMovieCardData,
          poster: '',
        }}
      />
    </LimitWidth>
  );
};

export const LongTitle = () => {
  return (
    <LimitWidth>
      <MovieCard
        {...{
          ...sampleMovieCardData,
          title: 'This is a very long title,This is a very long title,This is a very long title,This is a very long title,This is a very long title,This is a very long title,This is a very long title,This is a very long title,This is a very long title,This is a very long title,This is a very long title,This is a very long title,',
        }}
      />
    </LimitWidth>
  );
};

export const Grid = () => {
  return (
    <div className="grid sm:grid-cols-2 md:grid-cols-3 xl:grid-cols-4 w-full gap-8 px-2">
      {
        Array.from(Array(8).keys()).map((i) => {
          return (
            <MovieCard
              key={i}
              {...sampleMovieCardData}
            />
          );
        })
      }
    </div>
  );
};

interface MovieResponseList {
  poster_path: string
  title: string
  release_date: string
  vote_average: number
  id: number
}

export const FetchData = () => {
  const [movies, setMovies] = useState<MovieResponseList[] | null>(null);

  useEffect(() => {
    const fetchMovies = async () => {
      try {
        const res = await fetch(`${MOVIE_BASE_URL}/popular?adult=false&api_key=${import.meta.env.NEXT_PUBLIC_THEMOVIEDB_API_KEY}&page=1&sort_by=popularity.desc`);
        const { results } = await res.json();
        setMovies(results);
      } catch (error) {
        console.log(error);
      }
    };

    fetchMovies();
  }, []);

  return (
    <div className="grid sm:grid-cols-2 md:grid-cols-3 xl:grid-cols-4 w-full gap-6 md:gap-8 gap-y-10 px-2">
      {
        movies?.map((movie) => {
          return (
            <MovieCard
              key={movie.id}
              poster={movie.poster_path}
              title={movie.title}
              releaseDate={movie.release_date}
              voteAverage={movie.vote_average}
              movieId={movie.id}
              unoptimizeImage
            />
          );
        })
      }
    </div>
  );
};

export default {
  title: 'Movie Card',
};
