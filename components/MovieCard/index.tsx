import React from 'react';
import Link from 'next/link';
import Image from 'next/image';
import { Card } from 'antd';
import { getMovieYear } from '../../utils/date';
import { getPosterUrl } from '../../utils/url';
import { rateColors } from '../../constants/colors';
import useFallbackImageSrc from '../../hooks/useFallbackImageSrc';

interface MovieCardProps {
  poster: string
  title: string
  releaseDate: string
  voteAverage: number | string
  movieId: string | number
  unoptimizeImage: boolean
}

const getRatingColor = (rate: number | string) => {
  if (rate >= 7) {
    return rateColors.high;
  }

  if (rate >= 6 && rate < 7) {
    return rateColors.mid2;
  }

  if (rate >= 4 && rate < 6) {
    return rateColors.mid;
  }

  if (rate >= 3 && rate < 4) {
    return rateColors.low2;
  }

  return rateColors.low;
};

const MovieCard = ({
  poster,
  title = 'No Title',
  releaseDate,
  voteAverage = 'NR',
  movieId = '/',
  unoptimizeImage = false,
}: Partial<MovieCardProps>) => {
  const { src, fallback, fallbackUnoptimized } = useFallbackImageSrc(getPosterUrl(poster));

  return (
    <Link href={movieId === '/' ? '/' : `/movie/${movieId.toString()}`}>
      <Card
        className="card__container"
        hoverable
        cover={(
          <div className="card__cover">
            <Image
              unoptimized={unoptimizeImage || fallbackUnoptimized}
              objectFit="cover"
              layout="fill"
              alt="example"
              src={src as string}
              onError={fallback}
            />

            {/* Vote Average */}
            <div
              style={{ backgroundColor: getRatingColor(voteAverage) }}
              className="card__vote"
            >
              {voteAverage}
            </div>
          </div>
        )}
      >
        <Card.Meta
          className="card__meta"
          title={title}
          description={getMovieYear(releaseDate)}
        />
      </Card>
    </Link>
  );
};

export default MovieCard;
