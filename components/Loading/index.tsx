import React from 'react';
import { Spin } from 'antd';

const Loading = () => {
  return (
    <div className="loading__wrapper">
      <Spin size="large" />
      <p>Loading...</p>
    </div>
  );
};

export default Loading;
