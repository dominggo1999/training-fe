import MovieList from '.';
import useMovies, { MovieData } from '../../hooks/useMovies';

export const Basic = () => {
  const { data: movies } = useMovies();

  return (
    <MovieList movies={movies as MovieData[]} />
  );
};

export default {
  title: 'Movie List',
};
