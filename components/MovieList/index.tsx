import React from 'react';
import { MovieData } from '../../hooks/useMovies';
import MovieCard from '../MovieCard';

interface MovieDataProps {
  movies: MovieData[]
}

const MovieList = ({ movies }: Partial<MovieDataProps>) => {
  return (
    <div className="movie-list">
      {movies?.map((movie) => {
        return (
          <MovieCard
            key={movie.id}
            poster={movie.poster_path}
            title={movie.title}
            releaseDate={movie.release_date}
            voteAverage={movie.vote_average}
            movieId={movie.id}
            unoptimizeImage
          />
        );
      })}
    </div>
  );
};

export default MovieList;
