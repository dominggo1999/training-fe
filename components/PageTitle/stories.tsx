import PageTitle from '.';

export const Default = () => {
  return (
    <PageTitle>
      This is a basic page title
    </PageTitle>
  );
};

export default {
  title: 'Page Title',
};
