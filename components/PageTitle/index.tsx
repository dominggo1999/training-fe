import React from 'react';
import { Typography } from 'antd';

interface PageTitleProps {
  children: React.ReactElement | string
}

const PageTitle = ({ children }: Partial<PageTitleProps>) => {
  return (
    <Typography.Title
      level={1}
      className="title"
    >
      {children}
    </Typography.Title>
  );
};

export default PageTitle;
