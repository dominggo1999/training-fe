// Ex : 2022-06-24 ==> 2022
export const getMovieYear = (date?: string) => {
  return date ? date.split('-')[0] : null;
};
