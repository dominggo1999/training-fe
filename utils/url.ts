import { POSTER_BASE_URL } from '../constants/endpoints';

export const getPosterUrl = (path?: string) => {
  if (!path) {
    return '/images/failed-image.png';
  }

  return POSTER_BASE_URL + path;
};
