export const MOVIE_BASE_URL = 'https://api.themoviedb.org/3/movie';
export const POSTER_BASE_URL = 'https://image.tmdb.org/t/p/w500';
