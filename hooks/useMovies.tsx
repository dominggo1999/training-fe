import React from 'react';
import { useQuery } from '@tanstack/react-query';
import { MOVIE_BASE_URL } from '../constants/endpoints';
import { THEMOVIEDB_API_KEY } from '../constants/credentials';

export interface MovieData {
  adult: boolean;
  backdrop_path: string;
  genre_ids: number[];
  id: number;
  original_language: string;
  original_title: string;
  overview: string;
  popularity: number;
  poster_path: string;
  release_date: string;
  title: string;
  video: boolean;
  vote_average: number;
  vote_count: number;
}

const useMovies = () => {
  const getMovies = async () => {
    return fetch(`${MOVIE_BASE_URL}/popular?adult=false&api_key=${THEMOVIEDB_API_KEY}&page=1&sort_by=popularity.desc`)
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        return data.results || [] as MovieData[];
      })
      .catch((error) => {
        throw new Error(error);
      });
  };

  const query = useQuery(['todos'], getMovies);

  return query;
};

export default useMovies;
