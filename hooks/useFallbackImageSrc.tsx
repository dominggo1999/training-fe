import React, { useState } from 'react';

const useFallbackImageSrc = (url: string, alternative = '/images/failed-image.png') => {
  const [src, setSrc] = useState<string>(url);
  const [fallbackUnoptimized, setFallbackUnoptimized] = useState<boolean>(false);

  const fallback: React.ReactEventHandler<HTMLImageElement> = () => {
    setSrc(alternative);
    setFallbackUnoptimized(true);
  };

  return { src, fallback, fallbackUnoptimized };
};

export default useFallbackImageSrc;
