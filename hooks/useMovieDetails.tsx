import React from 'react';
import { useRouter } from 'next/router';
import useMovies, { MovieData } from './useMovies';

const useMovieDetails = () => {
  const { isLoading, isError, data: movies } = useMovies();
  const router = useRouter();
  const { movieId } = router.query;

  const getDetails = () => {
    if (movieId) {
      const currentMovie = (movies as MovieData[]).filter((movie) => {
        if (typeof movieId === 'string') {
          return movie.id === parseInt(movieId as string, 10);
        }
        return false;
      })[0];

      // Redirect to not found page if there is movie found in cache
      if (!currentMovie) {
        router.push('/404');
      }

      return currentMovie;
    }
  };

  return {
    isLoading, isError, getDetails,
  };
};

export default useMovieDetails;
