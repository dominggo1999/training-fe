import type { GlobalProvider } from '@ladle/react';
import {
  QueryClientProvider,
  QueryClient,
} from '@tanstack/react-query';
import { persistQueryClient, removeOldestQuery } from '@tanstack/react-query-persist-client';
import { createSyncStoragePersister } from '@tanstack/query-sync-storage-persister';
import 'antd/dist/antd.css';
import '../styles/main.scss';
import Image from 'next/image';

Object.defineProperty(Image, 'default', {
  configurable: true,
  value: (props) => (
    <Image
      {...props}
      unoptimized
    />
  ),
});

// Create a client
const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      cacheTime: 1000 * 60 * 60 * 24, // 24 hours
    },
  },
});

const localStoragePersister = createSyncStoragePersister({
  storage: window.localStorage,
  retry: removeOldestQuery,
});

persistQueryClient({
  queryClient,
  persister: localStoragePersister,
});

export const Provider: GlobalProvider = ({ children, globalState }) => (
  <QueryClientProvider client={queryClient}>
    {children}
  </QueryClientProvider>

);
